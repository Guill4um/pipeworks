local fs_helpers = pipeworks.fs_helpers

local tagsize_max = tonumber(core.settings:get("pipeworks_tag_max_size")) or  100

if pipeworks.enable_mese_tube then
	local function update_formspec(pos)
		local meta = minetest.get_meta(pos)
		local old_formspec = meta:get_string("formspec")
		if string.find(old_formspec, "button1") then -- Old version
			local inv = meta:get_inventory()
			for i = 1, 6 do
				for _, stack in ipairs(inv:get_list("line"..i)) do
					minetest.add_item(pos, stack)
				end
			end
		end
		local buttons_formspec = ""
		for i = 0, 5 do
			buttons_formspec = buttons_formspec .. fs_helpers.cycling_button(meta,
				"image_button[9,"..(i+0.2)..";1,0.6", "l"..(i+1).."s",
				{
					pipeworks.button_off,
					pipeworks.button_on
				}
			)
		end
		meta:set_string("formspec", 
			"size[10,10.2]"..
			"list[context;line1;1,0;6,1;] field[7.3,0.3;2,1;tag1;Tag;${tag1}]"..
			"list[context;line2;1,1;6,1;] field[7.3,1.3;2,1;tag2; ;${tag2}]"..
			"list[context;line3;1,2;6,1;] field[7.3,2.3;2,1;tag3; ;${tag3}]"..
			"list[context;line4;1,3;6,1;] field[7.3,3.3;2,1;tag4; ;${tag4}]"..
			"list[context;line5;1,4;6,1;] field[7.3,4.3;2,1;tag5; ;${tag5}]"..
			"list[context;line6;1,5;6,1;] field[7.3,5.3;2,1;tag6; ;${tag6}]"..
			"image[0,0;1,1;pipeworks_white.png]"..
			"image[0,1;1,1;pipeworks_black.png]"..
			"image[0,2;1,1;pipeworks_green.png]"..
			"image[0,3;1,1;pipeworks_yellow.png]"..
			"image[0,4;1,1;pipeworks_blue.png]"..
			"image[0,5;1,1;pipeworks_red.png]"..
			"image[8.5,6.3;1,1;pipeworks_mese_tube_noctr_6.png]"..
			"label[8.3,7.3;Help on Tag ?]"..
			"tooltip[8,6.3;2,2; \nItem Tag"..":\n"..
			"\n\tYou can choose output tags (max lenght:"..tostring(tagsize_max) ..")"..
			"\n\t\t - the stack go to the output color where its tag match the output one"..
			"\n\t\t - you can choose several tags separated by commas per output"..
			"\n\t\t - the tag on a stack can be set with filter-injectors.\n.]"..
			buttons_formspec..
			"list[current_player;main;0,6.3;8,4;]" ..
			"listring[current_player;main]" ..
			"listring[current_player;main]" ..
			"listring[context;line1]" ..
			"listring[current_player;main]" ..
			"listring[context;line2]" ..
			"listring[current_player;main]" ..
			"listring[context;line3]" ..
			"listring[current_player;main]" ..
			"listring[context;line4]" ..
			"listring[current_player;main]" ..
			"listring[context;line5]" ..
			"listring[current_player;main]" ..
			"listring[context;line6]"
			)
	end

	pipeworks.register_tube("pipeworks:mese_tube", {
			description = "Sorting Pneumatic Tube Segment",
			inventory_image = "pipeworks_mese_tube_inv.png",
			noctr = {"pipeworks_mese_tube_noctr_1.png", "pipeworks_mese_tube_noctr_2.png", "pipeworks_mese_tube_noctr_3.png",
				"pipeworks_mese_tube_noctr_4.png", "pipeworks_mese_tube_noctr_5.png", "pipeworks_mese_tube_noctr_6.png"},
			plain = {"pipeworks_mese_tube_plain_1.png", "pipeworks_mese_tube_plain_2.png", "pipeworks_mese_tube_plain_3.png",
				"pipeworks_mese_tube_plain_4.png", "pipeworks_mese_tube_plain_5.png", "pipeworks_mese_tube_plain_6.png"},
			ends = { "pipeworks_mese_tube_end.png" },
			short = "pipeworks_mese_tube_short.png",
			no_facedir = true,  -- Must use old tubes, since the textures are rotated with 6d ones
			node_def = {
				tube = {can_go = function(pos, node, velocity, stack)
						 local tbl, tbln = {}, 0
						 local found, foundn = {}, 0
						 local meta = minetest.get_meta(pos)
						 local inv = meta:get_inventory()
						 local name = stack:get_name()
						 local tag = stack:get_meta():get_string("tag")
						 for i, vect in ipairs(pipeworks.meseadjlist) do
							local npos = vector.add(pos, vect)
							local node = minetest.get_node(npos)
							local reg_node = minetest.registered_nodes[node.name]
							local lineTag = meta:get_string("tag"..tostring(i))
							if lineTag == nil then lineTag = "" end
							if meta:get_int("l"..i.."s") == 1 and reg_node then
								local tube_def = reg_node.tube
								if not tube_def or not tube_def.can_insert or
								tube_def.can_insert(npos, node, stack, vect) then
									local invname = "line"..i
									local is_empty = true
									-- filter by tag
									local isMatch = false
									if lineTag ~= "" then is_empty = false end
									if not is_empty then 
										lineTag = ","..lineTag..","
										for token in string.gmatch(lineTag, ",%s*"..tag.."%s*,") do isMatch = true end
									end
									if not is_empty and isMatch then 
										foundn = foundn + 1
										found[foundn] = vect
									else
										-- filter by stack node name
										for _, st in ipairs(inv:get_list(invname)) do
											if not st:is_empty() then
												is_empty = false
												if st:get_name() == name then
													foundn = foundn + 1
													found[foundn] = vect
												end
											end
										end
									end
									if is_empty then
										tbln = tbln + 1
										tbl[tbln] = vect
									end
								end
							 end
						 end
						 return (foundn > 0) and found or tbl
					end},
				on_construct = function(pos)
					local meta = minetest.get_meta(pos)
					local inv = meta:get_inventory()
					for i = 1, 6 do
						meta:set_int("l"..tostring(i).."s", 1)
						inv:set_size("line"..tostring(i), 6*1)
					end
					update_formspec(pos)
					meta:set_string("infotext", "Sorting pneumatic tube")
				end,
				on_punch = update_formspec,
				on_receive_fields = function(pos, formname, fields, sender)
					if not pipeworks.may_configure(pos, sender) then return end

					local meta = minetest.get_meta(pos)
					for i = 1, 6 do
						if  fields["tag"..tostring(i)] then
							meta:set_string("tag"..tostring(i),fields["tag"..tostring(i)])
						end
					end
			
					fs_helpers.on_receive_fields(pos, fields)
					update_formspec(pos)
				end,
				can_dig = function(pos, player)
					update_formspec(pos) -- so non-virtual items would be dropped for old tubes
					return true
				end,
				allow_metadata_inventory_put = function(pos, listname, index, stack, player)
					if not pipeworks.may_configure(pos, player) then return 0 end
					update_formspec(pos) -- For old tubes
					local inv = minetest.get_meta(pos):get_inventory()
					local stack_copy = ItemStack(stack)
					stack_copy:set_count(1)
					inv:set_stack(listname, index, stack_copy)
					return 0
				end,
				allow_metadata_inventory_take = function(pos, listname, index, stack, player)
					if not pipeworks.may_configure(pos, player) then return 0 end
					update_formspec(pos) -- For old tubes
					local inv = minetest.get_meta(pos):get_inventory()
					inv:set_stack(listname, index, ItemStack(""))
					return 0
				end,
				allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
					if not pipeworks.may_configure(pos, player) then return 0 end
					update_formspec(pos) -- For old tubes
					local inv = minetest.get_meta(pos):get_inventory()

					if from_list:match("line%d") and to_list:match("line%d") then
						return count
					else
						inv:set_stack(from_list, from_index, ItemStack(""))
						return 0
					end
				end,
			},
	})

	minetest.register_craft( {
		output = "pipeworks:mese_tube_000000 2",
		recipe = {
			{ "basic_materials:plastic_sheet", "basic_materials:plastic_sheet", "basic_materials:plastic_sheet" },
			{ "", "default:mese_crystal", "" },
			{ "basic_materials:plastic_sheet", "basic_materials:plastic_sheet", "basic_materials:plastic_sheet" }
		},
	})

	minetest.register_craft( {
		type = "shapeless",
		output = "pipeworks:mese_tube_000000",
		recipe = {
			"pipeworks:tube_1",
			"default:mese_crystal_fragment",
			"default:mese_crystal_fragment",
			"default:mese_crystal_fragment",
			"default:mese_crystal_fragment"
		},
	})
end
